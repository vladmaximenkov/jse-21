package ru.vmaksimenkov.tm.util;

import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String dashedLine() {
        StringBuilder sb = new StringBuilder(20);
        for (int n = 0; n < 20; ++n)
            sb.append('-');
        sb.append(System.lineSeparator());
        return sb.toString();
    }

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}
