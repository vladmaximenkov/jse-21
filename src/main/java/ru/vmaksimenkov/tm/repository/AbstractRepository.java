package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(E e) {
        list.add(e);
    }

    @Override
    public void clear(final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(list::remove);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return list.stream().anyMatch(e -> userId.equals(e.getUserId()) && id.equals(e.getId()));
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String userId, final String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void remove(final E e) {
        list.remove(e);
    }

    @Override
    public void removeById(final String userId, final String id) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public int size(String userId) {
        return (int) list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .count();
    }

}
