package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void bindTaskPyProjectId(final String userId, final String projectId, final String taskId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && taskId.equals(e.getId()))
                .findFirst()
                .ifPresent(e -> e.setProjectId(projectId));
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        return list.stream()
                .anyMatch(e -> userId.equals(e.getUserId()) && name.equals(e.getName()));
    }

    @Override
    public boolean existsByProjectId(final String userId, final String projectId) {
        return list.stream()
                .anyMatch(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()));
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getIdByIndex(final String userId, final Integer index) {
        final Task task = list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
        return task != null ? task.getId() : null;
    }

    @Override
    public void removeAllBinded(final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && !isEmpty(e.getProjectId()))
                .forEach(this::remove);
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .forEach(this::remove);
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        remove(list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        remove(list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String taskId) {
        findById(userId, taskId).setProjectId(null);
    }

}
