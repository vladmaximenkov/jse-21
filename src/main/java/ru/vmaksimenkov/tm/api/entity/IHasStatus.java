package ru.vmaksimenkov.tm.api.entity;

import ru.vmaksimenkov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
